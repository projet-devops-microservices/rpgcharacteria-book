# Étape 1 : Construction du projet avec Maven
FROM maven:3.8.4-openjdk-17 AS build
WORKDIR /app
COPY pom.xml .
COPY src ./src
RUN mvn clean package -DskipTests

# Étape 2 : Construction de l'image runtime avec OpenJDK
FROM openjdk:17-jdk
WORKDIR /app
COPY --from=build /app/target/*.jar rpgcharacteria-book-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "rpgcharacteria-book-0.0.1-SNAPSHOT.jar"]
