package com.devops.image.generator.controller;

import com.devops.image.generator.dto.BookDto;
import com.devops.image.generator.service.BookManagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api")
public class BookController {
    private BookManagement bookManagement;
    @Autowired
    public BookController(BookManagement bookManagement) {
        this.bookManagement = bookManagement;
    }

    @GetMapping("/characters")
    public List<BookDto> findAllBooks(){
        return bookManagement.findAllBooks();
    }

    @PostMapping("/characters")
    public List<BookDto> saveBook(@RequestBody List<BookDto> bookDtos){return bookManagement.createBooks(bookDtos);}

}
