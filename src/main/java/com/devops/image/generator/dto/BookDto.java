package com.devops.image.generator.dto;

public record BookDto(String description,String imageUrl) {}