package com.devops.image.generator.dto;


import com.devops.image.generator.entity.Book;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BookMapper {
    BookDto toDto(Book book);
    List<BookDto> toDtos(List<Book> books);
    Book toEntity(BookDto bookDto);

}
