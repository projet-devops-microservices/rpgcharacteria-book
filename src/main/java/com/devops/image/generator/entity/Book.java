package com.devops.image.generator.entity;

import jakarta.persistence.*;

@Entity
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "text")
    private String description;

    @Column(columnDefinition = "text")
    private String imageUrl;

    protected Book() {
    }

    public Book(String description, String imageUrl) {
        if(description==null || description.isEmpty()){
            throw new IllegalArgumentException("The description is obligatory");
        }
        if(imageUrl==null || imageUrl.isEmpty()){
            throw new IllegalArgumentException("The imageUrl is obligatory");
        }
        this.description = description;
        this.imageUrl = imageUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
