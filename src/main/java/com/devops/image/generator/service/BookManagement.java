package com.devops.image.generator.service;

import com.devops.image.generator.dto.BookDto;
import com.devops.image.generator.dto.BookMapper;
import com.devops.image.generator.entity.Book;
import com.devops.image.generator.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookManagement {
    private BookRepository bookRepository;
    private BookMapper bookMapper;
    @Autowired
    public BookManagement(BookRepository bookRepository, BookMapper bookMapper) {
        this.bookMapper = bookMapper;
        this.bookRepository = bookRepository;
    }
    public List<BookDto> findAllBooks(){
        List<Book> books= this.bookRepository.findAll();
        return books.stream().map(book -> this.bookMapper.toDto(book))
                .collect(Collectors.toList());
    }

    public List<BookDto> createBooks(List<BookDto> bookDtos) {
        List<Book> books = bookDtos.stream()
                .map(bookMapper::toEntity)
                .collect(Collectors.toList());

        List<Book> savedBooks = bookRepository.saveAll(books);

        return savedBooks.stream()
                .map(bookMapper::toDto)
                .collect(Collectors.toList());
    }
}
