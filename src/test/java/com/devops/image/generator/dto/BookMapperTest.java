package com.devops.image.generator.dto;

import com.devops.image.generator.entity.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class BookMapperTest {

    @Autowired
    private BookMapper bookMapper;

    @Test
    void shouldMapBookToBookDto() {
        Book book = new Book("Description", "ImageURL");
        BookDto bookDto = bookMapper.toDto(book);

        assertNotNull(bookDto);
        assertEquals(book.getDescription(), bookDto.description());
        assertEquals(book.getImageUrl(), bookDto.imageUrl());
    }

    @Test
    void shouldMapBookListToBookDtoList() {
        List<Book> books = List.of(new Book("Description1", "ImageURL1"), new Book("Description2", "ImageURL2"));
        List<BookDto> bookDtos = bookMapper.toDtos(books);

        assertNotNull(bookDtos);
        assertEquals(books.size(), bookDtos.size());
        assertEquals(books.get(0).getDescription(), bookDtos.get(0).description());
        assertEquals(books.get(1).getImageUrl(), bookDtos.get(1).imageUrl());
    }
}
