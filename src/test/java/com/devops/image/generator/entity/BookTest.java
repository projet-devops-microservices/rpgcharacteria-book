package com.devops.image.generator.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BookTest {

    @BeforeEach
    void setUp() {
    }

    @Test
    void bookShouldBeCreated(){
        Book book = new Book("L'alien possède trois yeux, deux antennes sur la tête, et porte une tenue vive et dépareillée. Il est représenté dans une pose humoristique, ajoutant une touche ludique à la scène.",
                "https://files.oaiusercontent.com/file-ikE5IzKnlyroDoZCGAMqma8m?se=2024-03-28T22%3A16%3A07Z&sp=r&sv=2021-08-06&sr=b&rscc=max-age%3D31536000%2C%20immutable&rscd=attachment%3B%20filename%3Da697623c-a380-4d5f-81a1-9a137d96d5a0.webp&sig=uJFaKdeQ9DV80ssU3USKcdDKUUpSYbPi8JETSi6VXjo%3D");

        assertEquals(book.getDescription(),"L'alien possède trois yeux, deux antennes sur la tête, et porte une tenue vive et dépareillée. Il est représenté dans une pose humoristique, ajoutant une touche ludique à la scène.");
        assertEquals(book.getImageUrl(),"https://files.oaiusercontent.com/file-ikE5IzKnlyroDoZCGAMqma8m?se=2024-03-28T22%3A16%3A07Z&sp=r&sv=2021-08-06&sr=b&rscc=max-age%3D31536000%2C%20immutable&rscd=attachment%3B%20filename%3Da697623c-a380-4d5f-81a1-9a137d96d5a0.webp&sig=uJFaKdeQ9DV80ssU3USKcdDKUUpSYbPi8JETSi6VXjo%3D");
    }

    @Test
    void bookWithInvalideDescriptionShouldThrowError(){

        Throwable exception =assertThrows(IllegalArgumentException.class,()->{
            new Book(null,"https://files.oaiusercontent.com/file-ikE5IzKnlyroDoZCGAMqma8m?se=2024-03-28T22%3A16%3A07Z&sp=r&sv=2021-08-06&sr=b&rscc=max-age%3D31536000%2C%20immutable&rscd=attachment%3B%20filename%3Da697623c-a380-4d5f-81a1-9a137d96d5a0.webp&sig=uJFaKdeQ9DV80ssU3USKcdDKUUpSYbPi8JETSi6VXjo%3D");
        });

        assertSame("The description is obligatory",exception.getMessage());
    }

    @Test
    void bookWithEmptyDescriptionShouldThrowError(){

        Throwable exception =assertThrows(IllegalArgumentException.class,()->{
            new Book("","https://files.oaiusercontent.com/file-ikE5IzKnlyroDoZCGAMqma8m?se=2024-03-28T22%3A16%3A07Z&sp=r&sv=2021-08-06&sr=b&rscc=max-age%3D31536000%2C%20immutable&rscd=attachment%3B%20filename%3Da697623c-a380-4d5f-81a1-9a137d96d5a0.webp&sig=uJFaKdeQ9DV80ssU3USKcdDKUUpSYbPi8JETSi6VXjo%3D");
        });

        assertSame("The description is obligatory",exception.getMessage());
    }

    @Test
    void bookWithInvalideImageUrlShouldThrowError(){

        Throwable exception =assertThrows(IllegalArgumentException.class,()->{
            new Book("L'alien possède trois yeux, deux antennes sur la tête, et porte une tenue vive et dépareillée. Il est représenté dans une pose humoristique, ajoutant une touche ludique à la scène.",null);
        });

        assertSame("The imageUrl is obligatory",exception.getMessage());
    }

    @Test
    void bookWithEmptyImageUrlShouldThrowError(){

        Throwable exception =assertThrows(IllegalArgumentException.class,()->{
            new Book("L'alien possède trois yeux, deux antennes sur la tête, et porte une tenue vive et dépareillée. Il est représenté dans une pose humoristique, ajoutant une touche ludique à la scène.","");
        });

        assertSame("The imageUrl is obligatory",exception.getMessage());
    }
}