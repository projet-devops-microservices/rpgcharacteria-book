package com.devops.image.generator.service;

import com.devops.image.generator.dto.BookDto;
import com.devops.image.generator.dto.BookMapper;
import com.devops.image.generator.entity.Book;
import com.devops.image.generator.repository.BookRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

class BookManagementTest {

    @Mock
    private BookRepository bookRepository;
    @InjectMocks
    private BookManagement bookManagement;
    @Mock
    private BookMapper bookMapper;

    @BeforeEach
    void init(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void should_return_all_book() {

        BookDto bookDto1 = new BookDto("L'alien possède trois yeux, deux antennes sur la tête, et porte une tenue vive et dépareillée. Il est représenté dans une pose humoristique, ajoutant une touche ludique à la scène.",
                "https://files.oaiusercontent.com/file-ikE5IzKnlyroDoZCGAMqma8m?se=2024-03-28T22%3A16%3A07Z&sp=r&sv=2021-08-06&sr=b&rscc=max-age%3D31536000%2C%20immutable&rscd=attachment%3B%20filename%3Da697623c-a380-4d5f-81a1-9a137d96d5a0.webp&sig=uJFaKdeQ9DV80ssU3USKcdDKUUpSYbPi8JETSi6VXjo%3D");
        BookDto bookDto2 = new BookDto("L'alien possède plusieurs membres, arbore une apparence colorée et unique, et exprime une sérénité et une paix profondes. Il est entouré d'éléments futuristes et originaux qui suggèrent un environnement spatial.",
                "https://files.oaiusercontent.com/file-U9mbPBV0JBe24AUSIqIqjEb3?se=2024-03-28T22%3A45%3A58Z&sp=r&sv=2021-08-06&sr=b&rscc=max-age%3D31536000%2C%20immutable&rscd=attachment%3B%20filename%3D4a1157aa-bfe0-4c8e-8858-77bc4f05a856.webp&sig=jMnntPvCShpfBZ9%2BUvBGcj1giqTaQdXUqj55mLfMtCk%3D");

        Book book1 = new Book(bookDto1.description(), bookDto1.imageUrl());
        Book book2 = new Book(bookDto2.description(), bookDto2.imageUrl());
        List<Book> bookList = List.of(book1, book2);

        List<BookDto> expectedBookDtos = List.of(bookDto1, bookDto2);

        when(bookRepository.findAll()).thenReturn(bookList);
        when(bookMapper.toDto(book1)).thenReturn(bookDto1);
        when(bookMapper.toDto(book2)).thenReturn(bookDto2);

        List<BookDto> actualBookDtos = bookManagement.findAllBooks();

        assertEquals(expectedBookDtos.size(), actualBookDtos.size(), "The sizes of the book lists should be equal");
        assertEquals(expectedBookDtos, actualBookDtos, "The elements of the book lists should be equal");

    }

    @Test
    void createBooksTest() {
        BookDto bookDto1 = new BookDto("Description 1", "URL 1");
        BookDto bookDto2 = new BookDto("Description 2", "URL 2");
        List<BookDto> bookDtos = Arrays.asList(bookDto1, bookDto2);

        Book book1 = new Book("Description 1", "URL 1");
        Book book2 = new Book("Description 2", "URL 2");
        List<Book> books = Arrays.asList(book1, book2);

        when(bookMapper.toEntity(any(BookDto.class))).thenAnswer(i -> new Book(i.getArgument(0, BookDto.class).description(), i.getArgument(0, BookDto.class).imageUrl()));
        when(bookRepository.saveAll(anyList())).thenReturn(books);
        when(bookMapper.toDto(any(Book.class))).thenAnswer(i -> new BookDto(i.getArgument(0, Book.class).getDescription(), i.getArgument(0, Book.class).getImageUrl()));

        List<BookDto> result = bookManagement.createBooks(bookDtos);

        assertEquals(bookDtos.size(), result.size());
        assertEquals(bookDtos.stream().map(BookDto::description).collect(Collectors.toList()), result.stream().map(BookDto::description).collect(Collectors.toList()));
        assertEquals(bookDtos.stream().map(BookDto::imageUrl).collect(Collectors.toList()), result.stream().map(BookDto::imageUrl).collect(Collectors.toList()));
    }

}